---
layout: "post"
---



## Header1

---

> Quote lorem ipsum dolor sith ameth
>
> quote1

* list1
  * List2

<p style="color:#C5C5D2"> grey text </p>

White text

<= != <--

<div style = "text-align:center"> Middle </div>



<div style="text-align:right">right</div>





<p style="color:#AEACBE"> greyer text</p>

# Header1



`Code`

```
Code block
```

*bold*

**bold**

-strike-

~~strike~~

<u>underline</u>

:slightly_smiling_face:

<p style="color:red"> COLORED TEXT </p>



_underline_

- [] Tasl

- [ ] Task



[^footnote]



| First Header               | Second Header |
| -------------------------- | ------------- |
| thing and stufffffffffffff |               |
| Thing and other stuff      |               |



[^footnote]:  Description



foontone[^footnote]




$$
i*j = -i
$$


[an example](http://example.com/ "Title")

<h1 style="color:#85BDA6"> HeaderColored</h1>

<h2 style="color:#A9CEF4"> blue header</h2>

<p style="color:#51E5FF"> cyan</p>

<h3 style="color:#BFD7EA"> bean blue</h3>

<p style="color:#CBC5EA"> lavander </p>

<p style="color:#EAEAEA"> platinum </p>

<p style="color:#5688C7">glacoiusn</p>

<p style="color:#FA8334"> princeton orange </p>

<h4 style="color:#EC0B43"> magenta </h4>

$$
i*j = k
$$

longest line longest line longest 